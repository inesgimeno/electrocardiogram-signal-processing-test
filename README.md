# ELECTROCARDIOGRAM SIGNAL PROCESSING TEST

Electrocardiogram (ECG) signal pre-processing usually consist of several steps, being some of the most common the following:
• Recording spikes (artifacts of great amplitude) removal.
• Low-pass filtering.
• Powerline interference filtering.
• Baseli