function new_ecg = preproc(ecg, fs)
%% CODING LEVEL TEST
% Author: Inés Gimeno Molina
% Date: 06/10/2021
% Internship ITACA-COR/Corify

%% INPUTS
% ecg: Nx1 vector cointaining the ecg values 
% fs: sampling frequency in Hz

%% OUTPUTS
% new_ecg: preprocessed ecg
% figure with the preprocessed signal

%% CODE EXPLANATION
% This code is a basic code to preprocess a raw ECG:
%   1. Spikes removal: 1-D median filter to remove artefacts of great
%   amplitude
%   2. Low-pass filtering: cut-off freq at 150Hz (ecg range 0.05-150Hz)
%   3. Powerline interference filtering: notch filter to remove the
%   interferences at 50-60 Hz
%   4. Baseline wander and offset removal: high pass filter to remove
%   baseline wander(0.5Hz), this changes a bit the morfology of the signal,
%   and now the signal is centered at 0
%
%
% ALSO CHECKS IF THE CHANNEL PASSED HAS INFORMATION (NOT ALL 0s)

%% 0. Check if the channel is empty
if ecg == zeros(numel(ecg),1)
    f = msgbox('You cannot enter an empty channel');
    new_ecg =[];
else
    %% 1. Spikes removal
    t = (0:numel(ecg)-1)/fs;
    no_spikes_ecg = medfilt1(ecg,20);
    %% 2. Low-pass filter
    [b1,a1]=butter(5,150*2/fs,'low'); 
    low_pass_ecg=filtfilt(b1,a1,no_spikes_ecg);
    %% 3. Powerline interference filtering
    [b2, a2] = iirnotch(50*2/fs, 0.5);
    powerline_ecg = filtfilt(b2, a2, low_pass_ecg);
    %% 4. Baseline wander and offset removal
    [b3,a3] = butter(2, 0.5*2/fs,'high');
    new_ecg = filtfilt(b3, a3, powerline_ecg);

    figure
    plot(t,ecg,t,no_spikes_ecg, t, low_pass_ecg, t, powerline_ecg, t,new_ecg)
    legend('Original','Spikes removal', 'Low-pass filter', 'Powerline Interference removal', 'Baseline Wander correction')
    legend('boxoff')
end

end
